package com.lethargicstudio.choky.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.lethargicstudio.choky.R;

public class CreateWhoAmIRoom extends AppCompatActivity implements View.OnClickListener {

    EditText etRoomName;
    EditText etRoomPassword;
    Button btnCreateRoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_who_am_i_room_activity);

        etRoomName = findViewById(R.id.etWhoAmIRoomName);
        etRoomPassword = findViewById(R.id.etWhoAmIRoomPassword);
        btnCreateRoom = findViewById(R.id.btnWhoAmICreateRoom);
        btnCreateRoom.setOnClickListener(this);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = 700;

        this.getWindow().setAttributes(params);
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getApplicationContext(), WhoAmILobbyActivity.class);
        intent.putExtra("roomName", etRoomName.getText().toString());
        intent.putExtra("roomPassword", etRoomPassword.getText().toString());
        startActivity(intent);
    }
}
