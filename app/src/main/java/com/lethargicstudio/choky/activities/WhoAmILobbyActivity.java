package com.lethargicstudio.choky.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.lethargicstudio.choky.Objects.Player;
import com.lethargicstudio.choky.R;
import com.lethargicstudio.choky.adapters.PlayerListAdapter;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class WhoAmILobbyActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView rvPlayer;
    List<Player> playerList;
    Button btnStartGame;

    PlayerListAdapter playerListAdapter;

    private Socket socket;
    {
        try {
            socket = IO.socket("https://choky-server-johnybro.c9users.io");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.who_am_i_lobby_activity);

        socket.connect();

        Intent intent = getIntent();

        ((TextView)findViewById(R.id.tvWhoAmIRoomName)).setText(intent.getExtras().getString("roomName"));

        playerList = new ArrayList<>();
        playerList.add(new Player("Pseudo de test", "lol la description"));
        playerListAdapter = new PlayerListAdapter(playerList, getApplicationContext());

        rvPlayer = findViewById(R.id.rvWhoAmIPlayerList);
        rvPlayer.setAdapter(playerListAdapter);
        rvPlayer.setLayoutManager(new LinearLayoutManager(this));

        btnStartGame = findViewById(R.id.btnWhoAmIStartGame);
        btnStartGame.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        playerList.add(new Player("Pseudo de test", "lol la description"));
        playerListAdapter.notifyItemInserted(playerList.size() - 1);
    }
}
