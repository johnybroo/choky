package com.lethargicstudio.choky.Objects;

public class Player {
    public String nickname = "";
    public String description = "";

    public Player(String nickname, String description){
        this.nickname = nickname;
        this.description = description;
    }

    public Player(String nickname){
        this.nickname = nickname;
    }
}
