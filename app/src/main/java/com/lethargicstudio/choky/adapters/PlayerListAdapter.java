package com.lethargicstudio.choky.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lethargicstudio.choky.Objects.Player;
import com.lethargicstudio.choky.R;

import java.util.List;

public class PlayerListAdapter extends RecyclerView.Adapter<PlayerListAdapter.ViewHolder> {

    private List<Player> playerList = null;
    private Context context = null;

    public PlayerListAdapter(List<Player> playerList, Context context){
        this.playerList = playerList;
        this.context = context;
    }

    private Context getContext() {
        return context;
    }

    @Override
    public PlayerListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View playerView = inflater.inflate(R.layout.player_list_row, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(playerView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PlayerListAdapter.ViewHolder holder, int position) {
        Player player = playerList.get(position);
        holder.tvNickname.setText(player.nickname);
        holder.tvDescription.setText(player.description);
    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvNickname = null;
        public TextView tvDescription = null;

        public ViewHolder(View playerView) {
            super(playerView);

            this.tvNickname = playerView.findViewById(R.id.tvNickname);
            this.tvDescription = playerView.findViewById(R.id.tvDescription);
        }
    }
}
